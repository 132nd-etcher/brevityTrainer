# -*- coding: utf-8 -*-
'''
Created on 4 mars 2014

'''

import csv_file
import random
import codecs
import locale
import sys

class Questions(object):
    '''
    classdocs
    '''


    def __init__(self, in_file='brevity.csv'):
        '''
        Constructor
        '''
        self.d = {}
        csv = csv_file.CSV(in_file).read()
        for x in csv:
            self.d[x[0]] = x[1]
#         print(self.d)

    def pick_random(self):
        k = random.choice(self.d.keys())
        return k, self.d[k]

    def pick_random_but(self, l):
        for i in range(50):
            z, q = self.pick_random()
            if not q in l:
                return z, q
        return q

    def ask_regular(self, noise, stats):
        r = self.pick_random()
        q = r[0]
        a = r[1]
        pa = [a]
        while len(pa) < noise:
            pa.append(self.pick_random_but(pa)[1])
        random.shuffle(pa)
        print(q)
        for x in range(len(pa)):
            try:
                print('{}: {}'.format(x+1, codecs.encode(pa[x], sys.stdout.encoding)))
            except UnicodeEncodeError:
                print('{}: {}'.format(x+1, codecs.encode(pa[x], 'cp1252')))
        user_input = None
        while not isinstance(user_input, int) or user_input <= 0 or user_input > len(pa) :
            user_input = int(input('Select the correct anwser: '))
        if pa[user_input-1] == a:
            print('nice !\n')
            stats.correct(q)
        else:
            print('nope ...\n')
            stats.wrong(q)

    def ask_the_other_way_around(self, noise, stats):
        r = self.pick_random()
        q = r[1]
        a = r[0]
        try:
            print(codecs.encode(q, sys.stdout.encoding))
        except UnicodeEncodeError:
            print(codecs.encode(q, 'cp1252'))
        user_input = raw_input('Write the correct anwser: ')
        print(user_input)
        if user_input.upper() == a:
            print('nice !\n')
            stats.correct(a)
        else:
            print('nope ...\n')
            stats.wrong(a)

    def ask_target(self, noise, stats):
        r = self.pick_random()
        while not r[0] in stats.get_wrongs():
            r = self.pick_random()
        q = r[0]
        a = r[1]
        pa = [a]
        while len(pa) < noise:
            pa.append(self.pick_random_but(pa)[1])
        random.shuffle(pa)
        print(q)
        for x in range(len(pa)):
            try:
                print('{}: {}'.format(x+1, codecs.encode(pa[x], sys.stdout.encoding)))
            except UnicodeEncodeError:
                print('{}: {}'.format(x+1, codecs.encode(pa[x], 'cp1252')))
        user_input = None
        while not isinstance(user_input, int) or user_input <= 0 or user_input > len(pa) :
            user_input = int(input('Select the correct anwser: '))
        if pa[user_input-1] == a:
            print('nice !\n')
            stats.correct(q)
        else:
            print('nope ...\n')
            stats.wrong(q)

    def ask_target_the_other_way_around(self, noise, stats):
        r = self.pick_random()
        while not r[0] in stats.get_wrongs():
            r = self.pick_random()
        q = r[1]
        a = r[0]
        try:
            print(codecs.encode(q, sys.stdout.encoding))
        except UnicodeEncodeError:
            print(codecs.encode(q, 'cp1252'))
        user_input = raw_input('Write the correct anwser: ')
        print(user_input)
        if user_input.upper() == a:
            print('nice !\n')
            stats.correct(a)
        else:
            print('nope ...\n')
            stats.wrong(a)



