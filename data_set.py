# import csv

class DataSet():

    def __init__(self, init_list=None):
        if init_list:
            self.l = list(init_list)
        else:
            self.l = []

    def __len__(self):
        return len(self.l)

    # __iter__ is not strictly required, it's only needed to implement
    # efficient iteration.
    def __iter__(self):
        return self.l.__iter__()

    # __contains__ isn't strictly required either, it's only needed to
    # implement the `in` operator efficiently.
    def __contains__(self,item):
        return self.l.__contains__()

    def __getitem__(self, item):
        return self.l.__getitem__(item)

    # Mutable sequences only, provide the Python list methods.
    def append(self,item):
        self.l.append(item)
    def count(self, item):
        return self.l.count(item)
    def index(self,item):
        return self.l.index(item)
    def extend(self,other):
        return self.l.extend(other)
    def insert(self,index, item):
        return self.l.insert(index, item)
    def pop(self):
        return self.l.pop()
    def remove(self,item):
        return self.l.remove(item)
    def reverse(self):
        return self.l.reverse()

    def __eq__(self, other):
        return self.l == other.l

    def __str__(self):
        return self.l.__str__()

    def sort(self):
        self.l.sort()
        return self


