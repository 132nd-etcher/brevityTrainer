# -*- coding: utf-8 -*-
from cx_Freeze import setup, Executable
import sys

productName = "Brevity Trainer"
if 'bdist_msi' in sys.argv:
    sys.argv += ['--initial-target-dir', 'C:\InstallDir\\' + productName]
    sys.argv += ['--install-script', 'build.py']

exe = Executable(
      script="brevityTrainer.py",
#       base="Win32GUI",
      targetName="brevityTrainer.exe",
      )

includefiles = [
                ]
includes = [
            ]
excludes = ['pydoc', 'unittest', 'doctest', 'email']

options = {"build_exe": {
                       "includes":includes,
                       "include_files": includefiles,
                       "excludes": excludes
                       }
           }

setup(
      name="BrevityTrainer",
      version="0.1",
      author="RW-98 Etcher",
      description="Copyright 2013-2014",
      executables=[exe],
      options = options,
      scripts=[
               'build.py'
               ]
      )
